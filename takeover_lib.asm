    include takeover_inc.asm

gimeRegister0Value        export
set80x24TextMode          export
disableInterruptSources   export
restoreInterruptSources   export
swapRomsOut               export
swapRomsBackIn            export
safeMmuLogicalBlock       export
printChar80x24            export
updateRomCursor           export
_c_printString            export

    section code
    pragma  undefextern
    pragma  newsource

safeMmuLogicalBlock           fcb LOGICAL_4000_5FFF ; MMU block to use temporarily to access physical blocks
printChar80x24_CurrentCursorX fcb 0
printChar80x24_CurrentCursorY fcb 0
gimeRegister0Value            fcb %01001110         ; 0  = CoCo 3 Mode, 1  = CoCo 1/2 Compatible, 
                                                    ; 1  = MMU enabled (0 = disabled)
                                                    ; 0  = GIME IRQ disabled (1 = enabled)
                                                    ; 0  = GIME FIRQ disabled (1 = enabled)
                                                    ; 1  = Vector RAM at $FEXX enabled (0 = disabled)
                                                    ; 1  = Standard SCS (DISK) Normal (0 = expand)
                                                    ; 10 = ROM Map 32k Internal
                                                    ;      0x = 16K Internal, 16K External
                                                    ;      11 = 32K External - Except Interrupt Vectors

set80x24TextMode
    lda     #%00000011                  ; VideoMode        - BP(7), Unused(6), DESCEN(5), MOCH(4), H50(3), LPR2-LPR0=(2-0)
    sta     $FF98
    lda     #%00010101                  ; VideoResolution  - Unused(7), VRES1-VRES0(6-5), HRES2-HRES0(4-2), CRES1-CRES0(1-0)
    sta     $FF99
    lda     #%00010010                  ; BorderColor      - Unused=(7-6), R1(5), G1(4), B1(3), R0(2), G0(1), B0(0)
    sta     $FF9A
    lda     #%00000000                  ; VerticalScroll   - Unused=(7-4), SCEN(3), SC2-SC0(2-0)
    sta     $FF9C
    lda     #%11011000                  ; VerticalOffset1  - Physical address bits 18-3=0b1101100000000000000 (bits 2-0 always 0) == 0x6C000
    std     $FF9D
    lda     #%00000000                  ; VerticalOffset0  - 
    std     $FF9E
    lda     #%00000000                  ; HorizontalOffset - HE(6), X6-X0(6-0)
    sta     $FF9F
    lda     #%01001110                  ; CoCo 3 Mode, MMU Enabled, GIME IRQ disabled, GIME FIRQ disabled, Vector RAM at
    sta     gimeRegister0Value
    sta     $FF90                       ; $FEXX enabled, Standard SCS Normal, ROM Map 32k Internal
    rts

disableInterruptSources
    lda     $FF01
    anda    #$FE
    sta     $FF01                       ; Disable HSYNC IRQ

    lda     $FF03
    ora     #$01
    sta     $FF03                       ; Enable VSYNC IRQ

    lda     $FF21
    anda    #$FE
    sta     $FF21                       ; Disable Cassette Data FIRQ

    lda     $FF23
    anda    #$FE
    sta     $FF23                       ; Disable Cartridge FIRQ

    lda     #%01001110
    sta     gimeRegister0Value
    sta     $FF90                       ; 0  = CoCo 3 Mode, 1  = CoCo 1/2 Compatible, 
                                        ; 1  = MMU enabled (0 = disabled)
                                        ; 0  = GIME IRQ disabled (1 = enabled)
                                        ; 0  = GIME FIRQ disabled (1 = enabled)
                                        ; 1  = Vector RAM at $FEXX enabled (0 = disabled)
                                        ; 1  = Standard SCS (DISK) Normal (0 = expand)
                                        ; 10 = ROM Map 32k Internal
                                        ;      0x = 16K Internal, 16K External
                                        ;      11 = 32K External - Except Interrupt Vectors

    lda     #%00000000
    sta     $FF92                       ; Disable IRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    sta     $FF93                       ; Disable FIRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    rts

restoreInterruptSources
    lda     $FF03
    ora     #$01
    sta     $FF03                       ; Enable VSYNC IRQ

    lda     $FF01
    anda    #$FE
    sta     $FF01                       ; Disable HSYNC IRQ

    lda     $FF21
    anda    #$FE
    sta     $FF21                       ; Disable the Cassette Data FIRQ

    lda     $FF23
    anda    #$FE
    sta     $FF23                       ; Disable the Cartridge FIRQ

    lda     #%01001110
    sta     $FF90                       ; 0  = CoCo 3 Mode, 1  = CoCo 1/2 Compatible, 
                                        ; 1  = MMU enabled (0 = disabled)
                                        ; 0  = GIME IRQ disabled (1 = enabled)
                                        ; 0  = GIME FIRQ disabled (1 = enabled)
                                        ; 1  = Vector RAM at $FEXX enabled (0 = disabled)
                                        ; 1  = Standard SCS (DISK) Normal (0 = expand)
                                        ; 10 = ROM Map 32k Internal
                                        ;      0x = 16K Internal, 16K External
                                        ;      11 = 32K External - Except Interrupt Vectors

    lda     #%00000000
    sta     $FF92                       ; Disable IRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    sta     $FF93                       ; Disable FIRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    rts

swapRomsOut
    ldb     #PHYSICAL_60000_61FFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_0000_1FFF
    leax    _MMU+LOGICAL_0000_1FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_68000_69FFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_8000_9FFF
    leax    _MMU+LOGICAL_8000_9FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_6A000_6BFFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_A000_BFFF
    leax    _MMU+LOGICAL_A000_BFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_6C000_6DFFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_C000_DFFF
    leax    _MMU+LOGICAL_C000_DFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_6E000_6FFFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_E000_FFFF
    leax    _MMU+LOGICAL_E000_FFFF,pcr
    stb     ,x
    ;
    ldb     #LOGICAL_8000_9FFF
    stb     safeMmuLogicalBlock,pcr
    rts

swapRomsBackIn
    ldb     #PHYSICAL_70000_71FFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_0000_1FFF
    leax    _MMU+LOGICAL_0000_1FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_78000_79FFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_8000_9FFF
    leax    _MMU+LOGICAL_8000_9FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_7A000_7BFFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_A000_BFFF
    leax    _MMU+LOGICAL_A000_BFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_7C000_7DFFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_C000_DFFF
    leax    _MMU+LOGICAL_C000_DFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_7E000_7FFFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_E000_FFFF
    leax    _MMU+LOGICAL_E000_FFFF,pcr
    stb     ,x

    ldb     #LOGICAL_4000_5FFF
    stb     safeMmuLogicalBlock,pcr

    lbsr    updateRomCursor

    rts

_c_printString
    pshs    u
    ldx     4,s
    bsr     printString
    puls    u
    rts

; > x = pointer to string to print (zero terminated)
printString
    pshs    x,a
@next
    lda     ,x+
    beq     @done
    bsr     printChar80x24
    bra     @next
@done
    puls    a,x
    rts

printChar80x24
    pshs    x,b,a
    ; Map in $6C000 to our safeMmuLogicalBlock
    ldb     safeMmuLogicalBlock,pcr
    ldx     #MMU_BANK_REGISTERS_FIRST
    abx
    lda     #PHYSICAL_6C000_6DFFF
    sta     ,x                          ; Logical Block safeMmuLogicalBlock is now mapped to physical block $6C000
    ldx     _MMU,pcr
    abx
    ldb     ,x                          ; b = original physical bock mapped to safeMmuLogicalBlock
    ; Calculate the logical address for the next character
    lda     safeMmuLogicalBlock,pcr
    ldx     #$0000
@multiply
    tsta
    beq     @doneMultiply
    leax    $2000,x
    deca
    bra     @multiply
@doneMultiply
    lda     printChar80x24_CurrentCursorY,pcr
@adjustForY
    tsta
    beq     @doneAdjustForY
    leax    160,x
    deca
    bra     @adjustForY
@doneAdjustForY
    puls    a                           ; Get the character to print back into a
    pshs    b
    ldb     printChar80x24_CurrentCursorX,pcr
    abx
    abx
    ; Print the character
    cmpa    #13                         ; Newline?
    bne     @not_newline
    ; Print a newline (blank rest of line, move cursor down one and all the way to the left)
    lda     printChar80x24_CurrentCursorX,pcr
    ldb     #32                         ; space
@blankRestOfLine
    tsta
    beq     @doneBlankingLine
    stb     ,x+
    clr     ,x+
    deca
    bra     @blankRestOfLine
@doneBlankingLine
    inc     printChar80x24_CurrentCursorY,pcr ; Move down one line
    clr     printChar80x24_CurrentCursorX,pcr ; Move back to beginning of line
    bra     @donePrint
    ; Print the character
@not_newline
    sta     ,x+
    clr     ,x+
    inc     printChar80x24_CurrentCursorX,pcr ; Move one character to the right
    ;
@donePrint
    lda     printChar80x24_CurrentCursorX,pcr
    cmpa    #80                               ; Did we just print in the right-most column?
    blt     @doneAdjustX
    inc     printChar80x24_CurrentCursorY,pcr ; Move down one line
    clr     printChar80x24_CurrentCursorX,pcr ; Move back to beginning of line
@doneAdjustX
    lda     printChar80x24_CurrentCursorY,pcr
    cmpa    #24                               ; Did we just move off the bottom of the screen?
    blt     @doneAdjustY
    clr     printChar80x24_CurrentCursorY,pcr ; Move back to the top of the screen
@doneAdjustY
    ; Map the original physical block back into our safeMmuLogicalBlock
    ldb     safeMmuLogicalBlock,pcr
    ldx     #MMU_BANK_REGISTERS_FIRST
    abx
    puls    b
    stb     ,x
    ; Character printed.  We're out'a here!
    puls    b,x,pc

updateRomCursor:
        ldd     printChar80x24_CurrentCursorX   ;// Load x into a and y into b
        incb                                    ;// y is 1 based for ROMS, 0 based for us
        std     $FE02                           ;// Store x at $FF02 and y at $FF03
        pshs    a                               ;// Push x
        lda     #160
        mul                                     ;// d = 160 * y
        addb    ,s+                             ;// d += x
        adca    #0
        addd    #$2000                          ;// d += $2000
        std     $FE00                           ;// Store address of cursor at $FE00
        rts

    endsection
