#include "test.h"

int main()
{
    asm {
        lda     #%1111110
        sta     $FF90                       // 0  = CoCo 3 Mode (1 = CoCo 1/2 Compatible)
                                            // 1  = MMU enabled (0 = disabled)
                                            // 0  = GIME IRQ disabled (1 = enabled)
                                            // 0  = GIME FIRQ disabled (1 = enabled)
                                            // 0  = Vector RAM at $FEXX disabled (1 = enabled)
                                            // 1  = Standard SCS (DISK) Normal (0 = expand)
                                            // 10 = ROM Map 32k Internal
                                            //      (0x = 16K Internal, 16K External)
                                            //      (11 = 32K External - Except Interrupt Vectors)
    }
    waitForKeyPress();

    return 0;
}

void waitForKeyPress()
{
    while (inkey() > 0) {}
    while (inkey() == 0) {}
}
