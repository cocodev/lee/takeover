#ifndef _takeover_h_
#define _takeover_h_

#include <coco.h>

BOOL initializeDiskIo();
void disableInterruptSources();
void setupInterruptVectors();
void performTakeoverTests();
void doMemoryFillTest();
void doVSyncIrqTest();
void doVideoShiftTest();
void doDiskTest();
void mapRomsBackIn();
void mapRomsBackOut();
void waitForKeyPress(BOOL useRoms, const char* prompt);
void writeByteToMostOf64K(byte data);
void clearLinesToColor(void *startOfVideoMemory, word topLine, word bottomLine, byte color);
void drawColoredSpokes(void *startOfVideoMemory);
word shiftScreenTest();
void keyboardHandler();
void storePeekValueToDebugOutput(word peekAddress);
void storePeekWordValueToDebugOutput(word peekAddress);
void storeByteToDebugOutput(byte value);
void storeWordToDebugOutput(word value);
void storeDebugCountToStartOfDebugOutput();
BOOL isKeyDown(BOOL useRoms);
extern void c_printString(const char* text);

#define _PHYSICAL_00000_01FFF 0x00
#define _PHYSICAL_02000_03FFF 0x01
#define _PHYSICAL_04000_05FFF 0x02
#define _PHYSICAL_06000_07FFF 0x03
#define _PHYSICAL_08000_09FFF 0x04
#define _PHYSICAL_0A000_0BFFF 0x05
#define _PHYSICAL_0C000_0DFFF 0x06
#define _PHYSICAL_0E000_0FFFF 0x07
#define _PHYSICAL_10000_01FFF 0x08
#define _PHYSICAL_12000_13FFF 0x09
#define _PHYSICAL_14000_15FFF 0x0A
#define _PHYSICAL_16000_17FFF 0x0B
#define _PHYSICAL_18000_19FFF 0x0C

#define _PHYSICAL_50000_51FFF 0x28
#define _PHYSICAL_52000_53FFF 0x29
#define _PHYSICAL_54000_55FFF 0x2A
#define _PHYSICAL_56000_57FFF 0x2B
#define _PHYSICAL_58000_59FFF 0x2C
#define _PHYSICAL_5A000_5BFFF 0x2D
#define _PHYSICAL_5C000_5DFFF 0x2E
#define _PHYSICAL_5E000_5FFFF 0x2F
#define _PHYSICAL_60000_61FFF 0x30
#define _PHYSICAL_62000_63FFF 0x31
#define _PHYSICAL_64000_65FFF 0x32
#define _PHYSICAL_66000_67FFF 0x33
#define _PHYSICAL_68000_69FFF 0x34
#define _PHYSICAL_6A000_6BFFF 0x35
#define _PHYSICAL_6C000_6DFFF 0x36
#define _PHYSICAL_6E000_6FFFF 0x37
#define _PHYSICAL_70000_71FFF 0x38
#define _PHYSICAL_72000_73FFF 0x39
#define _PHYSICAL_74000_75FFF 0x3A
#define _PHYSICAL_76000_77FFF 0x3B
#define _PHYSICAL_78000_79FFF 0x3C
#define _PHYSICAL_7A000_7BFFF 0x3D
#define _PHYSICAL_7C000_7DFFF 0x3E
#define _PHYSICAL_7E000_7FFFF 0x3F

#define _MMU_BANK_REGISTERS_FIRST_  0xFFA0
#define _LOGICAL_0000_1FFF 0
#define _LOGICAL_2000_3FFF 1
#define _LOGICAL_4000_5FFF 2
#define _LOGICAL_6000_7FFF 3
#define _LOGICAL_8000_9FFF 4
#define _LOGICAL_A000_BFFF 5
#define _LOGICAL_C000_DFFF 6
#define _LOGICAL_E000_FFFF 7

#define mapPhysicalToLogicalMemory(physical, logical) \
    {poke(_MMU_BANK_REGISTERS_FIRST_ + logical, physical); MMU[logical] = physical;}

#define poke(address, byteToStore) *(byte *)(address) = (byte)(byteToStore)
#define pokeWord(address, wordToStore) *(word *)(address) = (word)(wordToStore)
#define peek(address) *(byte *)(address)
#define peekWord(address) *(word *)(address)

#endif /* _takeover_h_ */
