CMOC_COMPILE_SWITCHES = --coco --compile --intermediate
CMOC_LINK_SWITCHES = --coco --intermediate --org=2000 --limit=7800 -L/usr/local/share/cmoc/lib
CMOC_LIBRARY_SWITCHES = -lcmoc-crt-ecb -lcmoc-std-ecb

# 6592
# 3689
# 2388

all: ../takeover.dsk

%.o: ../%.c ../%.h
	cmoc $(CMOC_COMPILE_SWITCHES) -o $@ $<

%.o: ../%.asm
	cmoc $(CMOC_COMPILE_SWITCHES) -o $@ $<

t.bin: takeover.o takeover_lib.o diskRoutines.o
	cmoc $(CMOC_LINK_SWITCHES) -o t.bin takeover.o takeover_lib.o diskRoutines.o $(CMOC_LIBRARY_SWITCHES)

test.bin: test.o
	cmoc $(CMOC_LINK_SWITCHES) -o test.bin test.o $(CMOC_LIBRARY_SWITCHES)

../takeover.dsk: t.bin ../t.bas test.bin ../A.A
ifeq ("$(wildcard ../takeover.dsk)","")
	perl -e 'print chr(255) x (35*18*256)' > takeover.dsk
endif
	writecocofile --binary ../takeover.dsk t.bin
	writecocofile --binary ../takeover.dsk test.bin
	writecocofile --ascii ../takeover.dsk ../A.A
	writecocofile --ascii ../takeover.dsk ../t.bas
	
clean:
	find . -type f \( -name "*.o" -o -name "*.bin" -o -name "*.map" -o -name "*.link" -o -name "*.lst" -o -name "*.s" \) -delete
