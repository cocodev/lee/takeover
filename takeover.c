#include "takeover.h"

word timer = 0;
BOOL enableTimer = TRUE;
BOOL displayCharacters = FALSE;
byte character = 0;
byte keyColumnScans[8] = {0, 1, 2, 3, 4, 5, 6, 7};
byte keysDown[2] = {0, 0};
byte keyModifiers = 0;
word debugOffset = 2;
byte safeMmuScratchLogicalBlock = _LOGICAL_4000_5FFF;
byte MMU[8] = { 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3F };

int main()
{
    initCoCoSupport();
    if (initializeDiskIo() == FALSE)
    {
        storeDebugCountToStartOfDebugOutput();
        c_printString("Disk support not detected.");
        return -1;
    }
    asm("ldmd", "#1");
    setHighSpeed(TRUE);
    //setCgaPalette();
    asm("lbsr", "set80x24TextMode");
    void (*printChar80x24Hook)();
    asm
    {
        leax    printChar80x24,pcr
        stx     :printChar80x24Hook
    }
    void (*oldCHROOT)() = setConsoleOutHook(printChar80x24Hook);
    c_printString("v1.0\r");

    waitForKeyPress(TRUE, "Roms in, strarting takeover");

    disableInterrupts();
    asm("lbsr", "disableInterruptSources");
    asm("lbsr", "swapRomsOut");
    setupInterruptVectors();
    safeMmuScratchLogicalBlock = _LOGICAL_8000_9FFF;
    enableInterrupts();

    waitForKeyPress(FALSE, "Roms Out, about to call tests");
    performTakeoverTests();
    waitForKeyPress(FALSE, "Roms Out, done with tests");

    disableInterrupts();
    asm("lbsr", "swapRomsBackIn");
    asm("lbsr", "restoreInterruptSources");
    safeMmuScratchLogicalBlock = _LOGICAL_4000_5FFF;
    enableInterrupts();

    asm("lbsr", "set80x24TextMode");
    waitForKeyPress(TRUE, "Roms In, done with takeover");

    resetPalette(TRUE);
    setConsoleOutHook(oldCHROOT);
    setHighSpeed(FALSE);
    asm("ldmd", "#0");
    storeDebugCountToStartOfDebugOutput();

    return 0; // Everything should be restored, so a simple return should return to BASIC
    asm
    {
gimeRegister0Value  import
swapRomsOut         import
swapRomsBackIn      import
    }
}

void storePeekValueToDebugOutput(word peekAddress)
{
    byte originalPhysicalBlock = MMU[safeMmuScratchLogicalBlock];
    word logicalAddress = safeMmuScratchLogicalBlock * 0x2000 + 0x0F00;
    mapPhysicalToLogicalMemory(_PHYSICAL_6C000_6DFFF, safeMmuScratchLogicalBlock);
    poke(logicalAddress + debugOffset++, peek(peekAddress));
    mapPhysicalToLogicalMemory(originalPhysicalBlock, safeMmuScratchLogicalBlock);
    storeDebugCountToStartOfDebugOutput();
}

void storePeekWordValueToDebugOutput(word peekAddress)
{
    byte originalPhysicalBlock = MMU[safeMmuScratchLogicalBlock];
    word logicalAddress = safeMmuScratchLogicalBlock * 0x2000 + 0x0F00;
    mapPhysicalToLogicalMemory(_PHYSICAL_6C000_6DFFF, safeMmuScratchLogicalBlock);
    pokeWord(logicalAddress + debugOffset, peekWord(peekAddress));
    debugOffset += 2;
    mapPhysicalToLogicalMemory(originalPhysicalBlock, safeMmuScratchLogicalBlock);
    storeDebugCountToStartOfDebugOutput();
}

void storeByteToDebugOutput(byte value)
{
    storePeekValueToDebugOutput(&value);
}

void storeWordToDebugOutput(word value)
{
    storePeekWordValueToDebugOutput(&value);
}

void storeDebugCountToStartOfDebugOutput()
{
    byte originalPhysicalBlock = MMU[safeMmuScratchLogicalBlock];
    word logicalAddress = safeMmuScratchLogicalBlock * 0x2000 + 0x0F00;
    mapPhysicalToLogicalMemory(_PHYSICAL_6C000_6DFFF, safeMmuScratchLogicalBlock);
    pokeWord(logicalAddress, debugOffset - 2);
    mapPhysicalToLogicalMemory(originalPhysicalBlock, safeMmuScratchLogicalBlock);
}

void performTakeoverTests()
{
    //doMemoryFillTest();
    //doVSyncIrqTest();
    doDiskTest();
    //doVideoShiftTest();
}

// void doMemoryFillTest()
// {
//     for (word i = 5; i <= 255; i += 10) {
//         writeByteToMostOf64K((byte)i);
//     }
//
//     word originalPhysicalBlock = MMU[_LOGICAL_6000_7FFF];
//     mapPhysicalToLogicalMemory(_PHYSICAL_6C000_6DFFF, _LOGICAL_6000_7FFF);
//     asm
//     {
//         lda     #$20
//         sta     $6000
//         clra
//         sta     $6001
//         ldw     #$0EFE
//         ldx     #$6000
//         ldy     #$6002
//         tfm     x+,y+
//     }
//     mapPhysicalToLogicalMemory(_PHYSICAL_6C000_6DFFF, originalPhysicalBlock);
// }

// void doVSyncIrqTest()
// {
//     enableTimer = FALSE;
//     character = 0;
//     displayCharacters = TRUE;
//     enableTimer = TRUE;
//     while (character < 255) {}
//     displayCharacters = FALSE;
// }

// void doVideoShiftTest()
// {
//     byte gimeRegister0;
//     asm
//     {
//         lda     gimeRegister0Value,pcr
//         sta     :gimeRegister0
//     }
//     setVideoModeWithGimeRegister0(ModeGraphics320x200x16, gimeRegister0);
//     byte original6000 = MMU[_LOGICAL_6000_7FFF];
//     byte original8000 = MMU[_LOGICAL_8000_9FFF];
//     byte originalA000 = MMU[_LOGICAL_A000_BFFF];
//     byte originalC000 = MMU[_LOGICAL_C000_DFFF];
//     mapPhysicalToLogicalMemory(_PHYSICAL_56000_57FFF, _LOGICAL_6000_7FFF);
//     mapPhysicalToLogicalMemory(_PHYSICAL_58000_59FFF, _LOGICAL_8000_9FFF);
//     mapPhysicalToLogicalMemory(_PHYSICAL_5A000_5BFFF, _LOGICAL_A000_BFFF);
//     mapPhysicalToLogicalMemory(_PHYSICAL_5C000_5DFFF, _LOGICAL_C000_DFFF);
//
//     asm
//     {
//         pshs    y,x,b,a
//         ldd     #$2222
//         ldx     #$6000
//         ldy     #$4000
// @clearScreen
//         std     ,x++
//         leay    -1,y
//         bne     @clearScreen
//         puls    a,b,x,y
//     }
//
//     //clearGraphicsScreen(0x22);
//     clearLinesToColor(0x6000, 0, 142, 0x99);
//     clearLinesToColor(0x6000, 142, 152, 0x44);
//
//     drawColoredSpokes(0x6000);
//     mapPhysicalToLogicalMemory(_PHYSICAL_56000_57FFF, _LOGICAL_6000_7FFF);
//     word totalTime = shiftScreenTest();
//     for(word i = 0; i < 65535; i++) {}
//
//     mapPhysicalToLogicalMemory(original6000, _LOGICAL_6000_7FFF);
//     mapPhysicalToLogicalMemory(original8000, _LOGICAL_8000_9FFF);
//     mapPhysicalToLogicalMemory(originalA000, _LOGICAL_A000_BFFF);
//     mapPhysicalToLogicalMemory(originalC000, _LOGICAL_C000_DFFF);
//     setVideoMode(ModeText80x24);
//
//     // storeWordToDebugOutput(totalTime, _LOGICAL_6000_7FFF);
//}

void doDiskTest()
{
    disableInterrupts();
    asm("lbsr", "swapRomsBackIn");
    enableInterrupts();

    waitForKeyPress(TRUE, "Roms In, about to do disk test");

    char data[10];
    asm
    {
        pragma  undefextern
        pragma  newsource
        pshs    u
        ;
        ;// Open the file for input
        leax    testFilename,pcr
        lbsr    openFileForRead
        bcs     @error
        ;
        ;// Read 5 bytes into data
        ldx     #5
        stx     bytesToRead         ;// Number of bytes to read
        puls    u
        leax    data
        pshs    u
        stx     bytesReadBuffer     ;// Address of buffer to read into
        lbsr    readBytesFromFileIntoBuffer
        bcs     @error
        ;// Success!
        clra
        sta     ,y                  ;// Terminate the string with '\0'
        ;
        ;// Close the file
        lbsr    closeAllFiles
        ;
        puls    u
        bra     @done
@error
        puls    u
        ldx     #$2545              ;%E
        stx     data
        ldx     #$5252              ;RR
        stx     2+data
        ldx     #$4F52              ;OR
        stx     4+data
        clra
        sta     6+data              ;\0
        bra     @done
@done
        pragma  oldsource
    }
    c_printString("[");
    c_printString(data);
    c_printString("]\r");
    waitForKeyPress(TRUE, "Roms In, done with disk test");

    disableInterrupts();
    asm("lbsr", "swapRomsOut");
    enableInterrupts();

    return;
    asm
    {
testFilename    fcn 'A.A'
    }
}

void waitForKeyPress(BOOL useRoms, const char* prompt)
{
    if (prompt)
    {
        c_printString(prompt);
        c_printString("...\r");
    }
    else
    {
        c_printString("Press any key...");
    }
    
    while (isKeyDown(useRoms)) {}
    while (isKeyDown(useRoms) == FALSE) {}
}

//void drawColoredSpokes(void *startOfVideoMemory)
//{
//    byte color = 0;
//    word xCoordinates[] = { 0, 40, 80, 120, 159, 199, 239, 279, 319};
//    word yCoordinates[] = {    25, 50,  75,  99, 124, 149, 174     };
//    for (word index = 0; index < 9; ++index)
//    {
//        plotLine(startOfVideoMemory, 159, 99, xCoordinates[index], 0, color);
//        if (++color > 15)
//        {
//            color = 0;
//        }
//    }
//    for (word index = 0; index < 7; ++index)
//    {
//        plotLine(startOfVideoMemory, 159, 99, 319, yCoordinates[index], color);
//        if (++color > 15)
//        {
//            color = 0;
//        }
//    }
//    for (sword index = 8; index >= 0; --index)
//    {
//        plotLine(startOfVideoMemory, 159, 99, xCoordinates[index], 198, color);
//        if (++color > 15)
//        {
//            color = 0;
//        }
//    }
//    for (sword index = 6; index >= 1; --index)
//    {
//        plotLine(startOfVideoMemory, 159, 99, 0, yCoordinates[index], color);
//        if (++color > 15)
//        {
//            color = 0;
//        }
//    }
//}

//void clearLinesToColor(void *startOfVideoMemory, word topLine, word bottomLine, byte twoPixelColors)
//{
//    void *startAddress = startOfVideoMemory + (topLine * 160);
//    word count = (bottomLine - topLine) * 160;
//    memset(startAddress, twoPixelColors, count);
//}

//word shiftScreenTest()
//{
//    displayCharacters = FALSE;
//    timer = 0;
//    for(byte iterration = 0; iterration < 40; ++iterration)
//    {
//        asm {
//            ldx     #$6001
//            ldy     #$6000
//            lda     #0
//            ldb     #142
//    @copy:  ldw     #159
//            tfm     x+,y+
//            sta     ,y+
//            leax    1,x
//            decb
//            bne     @copy
//        }
//    }
//    
//    word totalTime = timer;
//
//    return totalTime;
//}

asm void setupInterruptVectors()
{
    asm {
        pshs    y,x
        ldx     #$0100
        lda     #$7E            ;// jmp
        leay    _interruptHandler,pcr
        ldb     6
@setVector
        sta     ,x+
        sty     ,x++
        decb
        bne     @setVector
        puls    x,y
    }
}

interrupt void interruptHandler()
{
    BOOL isVsync = FALSE;
    asm
    {
        tst     $FF03
        bpl     @doneVsync
        lda     #TRUE
        sta     :isVsync
@doneVsync
    }
    if (isVsync)
    {
        // if (enableTimer)
        // {
        //     if (displayCharacters)
        //     {
        //         //printf("%c", character++);
        //         asm {
        //             lda :character
        //         }
        //         hiresPutChar();
        //         character++;
        //     }
        //     timer++;
        // }

        asm {
            ldx     $FF02           // Acknowledge the VSYNC interrupt
            leax    _keyColumnScans,pcr
            lda     #%11111111
            sta     $FF02           // Disable all keyboard column strobes
            ;
            ldb     #%00000001      // Start off reading column 0
@loop
            comb
            stb     $FF02           // Enable the next column
            lda     $FF00           // Read the column
            coma                    // Invert bits so 1=key down, 0=key up
            sta     ,x+             // Store in next keyColumnScans element
            ;
            comb
            lslb                    // Next column. Shift bits in b to the left
            bne     @loop
        }
    }
}

//void writeByteToMostOf64K(byte data)
//{
//    // Test by storing data from $0000-$1FFF, $8000-$DFFF
//    // Exclude $2000-$7FFF because that's the blocks where the executing code and the stack are
//    // Exclude $E000-$FFFF because that's the black where the stack is
//    asm {
//        pshs    x
//        lda     :data
//        ldb     :data
//        tfr     d,w
//        ldx     #$112
//@fill_1
//        stq     ,x
//        stq     4,x
//        leax    8,x
//        cmpx    #$2000
//        ble     @fill_1
//        ;
//        ldx     #$8000
//@fill_2
//        stq     ,x
//        stq     4,x
//        leax    8,x
//        cmpx    #$E000
//        ble     @fill_2
//        ;
//        puls    x
//    }
//}

BOOL isKeyDown(BOOL useRoms)
{
    if (useRoms)
    {
        return inkey() > 0;
    }
    for(byte i = 0; i < 8; i++) {
        if (keyColumnScans[i] > 0) {
            return TRUE;
        }
    }
    return FALSE;
}
